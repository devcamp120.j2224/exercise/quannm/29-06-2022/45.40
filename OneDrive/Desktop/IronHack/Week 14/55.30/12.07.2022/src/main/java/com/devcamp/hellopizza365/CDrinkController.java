package com.devcamp.hellopizza365;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class CDrinkController {
    @CrossOrigin
    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrinks(){

        ArrayList<CDrink> listDrinks = new ArrayList<CDrink>();

        CDrink drink1 = new CDrink("TRATAC", "Trà tắc", 10000);
        CDrink drink2 = new CDrink("COCA", "Cocacola", 15000);
        CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000);
        CDrink drink4 = new CDrink("LAVIE", "Lavie", 5000);
        CDrink drink5 = new CDrink("TRASUA", "Trà sữa trân châu", 40000);
        CDrink drink6 = new CDrink("FANTA", "Fanta", 15000);

        listDrinks.add(drink1);
        listDrinks.add(drink2);
        listDrinks.add(drink3);
        listDrinks.add(drink4);
        listDrinks.add(drink5);
        listDrinks.add(drink6);

        return listDrinks;
    }
}
